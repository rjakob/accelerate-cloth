import Prelude hiding (init, concat)
import Data.Array.Accelerate as Acc
--import Data.Array.Accelerate.CUDA as C
import Data.Array.Accelerate.Interpreter as I
import Data.Array.Accelerate.Math.SMVM as SMVM
import Data.Array.Accelerate.Types
import Cloth
import Data.Bits ((.&.),(.|.))
import Debug.Trace


smvmTest = smvmMulti (segd,(idxs,vals),cnts) vecs
  where
    segd = use $ fromList (Z:.2:.3) [2,1,1,0,2,1] :: Acc (Array DIM2 Int)
    idxs = use $ fromList (Z:.2:.4) [0,2,2,0,1,2,0,0] :: Acc (Array DIM2 Int)
    vals = use $ fromList (Z:.2:.4) [1,5,3,2,1,1,1,0] :: Acc (Array DIM2 Float)
    cnts = use $ fromList (Z:.2) [3,3] :: Acc (Array DIM1 Int)
    vecs = use $ fromList (Z:.2:.3) [1,2,3,4,5,6] :: Acc (Array DIM2 Float)


-- TESTS --

testmpcgMulti2 n = mpcgMultiInitialAcc a b z p epsilon n
  where
    allsegs = use $ fromList (Z :. 2 :. 3) [3,3,3,3,3,3] :: AccMatrix Int
    allidxs = use $ fromList (Z :. 2 :. 9) [0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2] :: AccMatrix Int
    allvals = use $ fromList (Z :. 2 :. 9) [1.0,1.0,1.0,1.0,5.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,5.0,1.0,1.0,1.0,1.0] :: AccMatrix Float
    allcols = use $ fromList (Z :. 2) [3,3] :: AccVector Int
    --
    a = (allsegs,(allidxs,allvals),allcols)
    b = use $ fromList (Z :. 2 :. 3) [6,14,6,6,14,6] :: AccMatrix Float
    z = use $ fromList (Z :. 2 :. 3) [0,0,0,0,0,0] :: AccMatrix Float
    p = use $ fromList (Z :. 2 :. 3) [1,5,1,1,5,1] :: Acc (Array DIM2 Float)
    epsilon = 0.0000001

testmpcgMulti3 :: Int -> Int -> AccMatrix Float
testmpcgMulti3 m n = mpcgMultiInitialAcc a b z p epsilon n
  where
    repit a = Acc.replicate (lift (Z :. m :. All :: (Z:.Int:.All))) a
    --
    allsegs = repit $ use $ fromList (Z :. 3) [3,3,3] :: AccMatrix Int
    allidxs = repit $ use $ fromList (Z :. 9) [0,1,2,0,1,2,0,1,2] :: AccMatrix Int
    allvals = repit $ use $ fromList (Z :. 9) [1.0,1.0,1.0,1.0,5.0,1.0,1.0,1.0,1.0] :: AccMatrix Float
    allcols = Acc.replicate (index1 $ constant m) $ unit $ constant 3 :: AccVector Int
    --
    a = (allsegs,(allidxs,allvals),allcols)
    b = repit $ use $ fromList (Z :. 3) [6,14,6] :: AccMatrix Float
    z = repit $ use $ fromList (Z :. 3) [0,0,0] :: AccMatrix Float
    p = repit $ use $ fromList (Z :. 3) [1,5,1] :: Acc (Array DIM2 Float)
    epsilon = 0.0000001

-----------------------------------------------------------

test n = mpcgInitial a b z p e n
  where
    a = fromArrayZero $ fromList (Z :. (3 :: Int) :. (3 :: Int)) ([1,1,1,1,5,1,1,1,1] :: [Float])
    b = fromList (Z :. (3 :: Int)) ([6,14,6] :: [Float])
    z = fromList (Z :. (3 :: Int)) ([0,0,0] :: [Float]) 
    p = fromList (Z :. (3 :: Int)) ([1,5,1] :: [Float])
    e = 0.0000000001


testAcc n = mpcgInitialAcc a b z p e n
  where
    a = usesm $ fromArrayZero $ fromList (Z :. (3 :: Int) :. (3 :: Int)) ([1,1,1,1,5,1,1,1,1] :: [Float])
    b = use $ fromList (Z :. (3 :: Int)) ([6,14,6] :: [Float])
    z = use $ fromList (Z :. (3 :: Int)) ([0,0,0] :: [Float]) 
    p = use $ fromList (Z :. (3 :: Int)) ([1,5,1] :: [Float])
    e = 0.0000000001
----


segs = use $ fromList (Z :. 2 :. 3) [2,1,1,0,0,0] :: AccMatrix Int
idxs = use $ fromList (Z :. 2 :. 4) [0,2,2,0,0,0,0,0] :: AccMatrix Int
vals = use $ fromList (Z :. 2 :. 4) [1,5,3,2,0,0,0,0] :: AccMatrix Float
cols = use $ fromList (Z :. 2) [3,3] :: AccVector Int
vecs = use $ fromList (Z :. 2 :. 3) [1,2,3,4,5,6] :: AccMatrix Float


segs2 = use $ fromList (Z :. 2 :. 3) [0,0,0,0,0,0] :: AccMatrix Int
idxs2 = use $ fromList (Z :. 2 :. 0) [] :: AccMatrix Int
vals2 = use $ fromList (Z :. 2 :. 0) [] :: AccMatrix Float
cols2 = use $ fromList (Z :. 2) [3,3] :: AccVector Int
vecs2 = use $ fromList (Z :. 2 :. 3) [1,2,3,4,5,6] :: AccMatrix Float


segs3 = use $ fromList (Z :. 1 :. 3) [1,1,0] :: AccMatrix Int
idxs3 = use $ fromList (Z :. 1 :. 2) [1,1] :: AccMatrix Int
vals3 = use $ fromList (Z :. 1 :. 2) [1,2] :: AccMatrix Float
cols3 = use $ fromList (Z :. 1) [3] :: AccVector Int
vecs3 = use $ fromList (Z :. 1 :. 3) [1,2,3] :: AccMatrix Float

-----------------------

seg1 = use $ fromList (Z :. 3) [1,1,2] :: AccVector Int
idx1 = use $ fromList (Z :. 4) [2,1,0,2] :: AccVector Int
val1 = use $ fromList (Z :. 4) [1,1,1,1] :: AccVector Float

seg2 = use $ fromList (Z :. 3) [1,2,3] :: AccVector Int
idx2 = use $ fromList (Z :. 6) [1,0,1,0,1,2] :: AccVector Int
val2 = use $ fromList (Z :. 6) [1,1,1,1,1,1] :: AccVector Float

sm1 = (seg1, (idx1, val1), unit 3) :: AccSparseMatrix Float
sm2 = (seg2, (idx2, val2), unit 3) :: AccSparseMatrix Float

cmb = Acc.zipWith (.|.) (toBitVector idx1 seg1) (toBitVector idx2 seg2)

-- TODO extend to array of arbitrary shape.
toBitVector :: AccVector Int -> Acc (Segments Int) -> AccVector Int
toBitVector idx seg = foldSeg f 0 idx seg
    where
    f a1 a2 = a1 `setBit` a2

-- SWAR algorithm to count bits set.
bitcount :: Exp Int -> Exp Int
bitcount i = (((i'' + (i'' `shiftR` 4)) .&. 0x0F0F0F0F) * 0x01010101) `shiftR` 24
    where
    i' = i - ((i `shiftR` 1) .&. 0x55555555)
    i'' = (i' .&. 0x33333333) + ((i' `shiftR` 2) .&. 0x33333333)

bitVectorToSeg :: AccVector Int -> AccVector Int
bitVectorToSeg = Acc.map bitcount

-- unused...
bit3toAcc :: Exp Int -> AccVector Int
bit3toAcc b = Acc.generate (index1 3) f
    where
    f ix = let ix' = unindex1 ix in (?) (testBit b ix') (ix',-1)

-- Transforms a bit vector in a idx vector.
-- At the moment at most 3 entries per segment are
-- allowed, because a more general method could not
-- be found, yet.
bitVectorToIdx3 :: AccVector Int -> AccVector Int
bitVectorToIdx3 bv = Acc.filter pF $ Acc.flatten $ Acc.generate size gF -- Reduce by one dimension, not flatten
    where
    pF a = a /=* -1
    size = index2 3 3 :: Exp (Z :. Int :. Int)
    gF ix = let (i1,i2) = unlift $ unindex2 ix in
           let b = bv ! index1 i1 in
           (?) (testBit b i2) (i2 ,-1)

-- TODO Unfinished!
smzipWith :: (Exp Float -> Exp Float -> Exp Float) -> AccSparseMatrix Float -> AccSparseMatrix Float -> AccSparseMatrix Float
smzipWith f (seg1,(idx1,val1),col1) (seg2,(idx2,val2),col2) = (seg, (idx, val), col)
    where
    bv1 = toBitVector idx1 seg1
    bv2 = toBitVector idx2 seg2
    all = Acc.zipWith (.|.) bv1 bv2
    com = Acc.zipWith (.&.) bv1 bv2
    seg = bitVectorToSeg all
    idx = bitVectorToIdx3 all
    col = col1
    val = Acc.filter (/=* -1) $ Acc.flatten template
    ---
    template = Acc.generate (index2 3 3 :: Exp (Z :. Int :. Int)) gF
    gF :: Exp (Z:.Int:.Int) -> Exp Float
    gF ix = let (i1,i2) = unlift $ unindex2 ix in
            let allbits = all ! index1 i1 in
            let combits = com ! index1 i1 in
            let l = (?) (testBit combits i2) (-5,-4) in
            (?) (testBit allbits i2) (l,-1)

-------------
-- Can we preprocess val such that it contains the position?
-- i.e. create a map to bitcounts?


-- TODO Maybe we should simply built up the
-- permutation and run permute once at the end. But this is probably equivalent in complexity.

oddEvenMergeSort :: (Elt a) => (Exp a -> Exp a -> Exp Bool) -> AccVector a -> AccVector a
oddEvenMergeSort cmp v = oddEvenMergeSortRange cmp 0 (size v) v

oddEvenMergeSortRange :: (Elt a) => (Exp a -> Exp a -> Exp Bool) -> Exp Int -> Exp Int -> AccVector a -> AccVector a
oddEvenMergeSortRange cmp lo hi v = (hi - lo >=* 1) ?| (v', v)
  where
  mid = lo + ((hi - lo) `div` 2)
  v' = oddEvenMerge cmp lo hi 1 $
       oddEvenMergeSortRange cmp (mid + 1) hi $
       oddEvenMergeSortRange cmp lo mid v

oddEvenMerge :: (Elt a) => (Exp a -> Exp a -> Exp Bool) -> Exp Int -> Exp Int -> Exp Int -> AccVector a -> AccVector a
oddEvenMerge cmp lo hi r v = (step <* hi - lo) ?| (v1, v2)
  where
  step = r * 2
  v1 = oddEvenMerge cmp (lo + r) hi step $
       oddEvenMerge cmp lo hi step v
--  oddeven_merge(x, lo, hi, step)
--       oddeven_merge(x, lo + r, hi, step)
--        for i in range(lo + r, hi - r, step):
--            compare_and_swap(x, i, i + r)
  v2 = compareAndSwap cmp lo (lo + r) v




v = use $ fromList (Z :. 8) [1,2,3,4,5,6,7,8] :: AccVector Int
v1 = use $ fromList (Z :. 4) [1,2,3,4] :: AccVector Int
v2 = use $ fromList (Z :. 4) [4,3,2,1] :: AccVector Int

v3 = use $ fromList (Z :. 8) [0,1,2,5,0,1,3,8] :: AccVector Int
v4 = use $ fromList (Z :. 8) [1,6,2,5,8,3,1,0] :: AccVector Int
v5 = use $ fromList (Z :. 16) [1,6,2,5,8,3,1,0,5,2,6,8,4,2,4,9] :: AccVector Int

-------------------------------------

concat :: (Elt a) => AccVector a -> AccVector a -> AccVector a
concat v1 v2 = generate ix gF
  where
  gF ix = let i = unindex1 ix in
          let s = size v1 in
          i <* s ? (v1 ! ix, v2 ! index1 (i - s))
  ix = index1 $ size v1 + size v2

sortCore :: (Elt a) => (Exp a -> Exp a -> Exp Bool) -> Int -> AccVector a -> AccVector a
sortCore lt len v 
	| len == 0	= use $ fromList (Z :. 0) []
	| len == 1	= v 
	| otherwise
	= let half = len `div` 2 in
      let s1 = slit 0 (lift half) v in
      let s2 = slit (lift half) (lift half) v
      in mergeCore lt len (sortCore lt half s1 `concat` sortCore lt half s2)



genPair a b = generate (index1 2) (\ix -> unindex1 ix ==* 0 ? (a, b))

-- | Length must be a value from the Haskell domain to ensure termination of
--   the recursion.
mergeCore :: (Elt a) => (Exp a -> Exp a -> Exp Bool) -> Int -> AccVector a -> AccVector a
mergeCore lt len xx = mergeCore' lt xx len 0 1

mergeCore' :: (Elt a) => (Exp a -> Exp a -> Exp Bool) -> AccVector a -> Int -> Exp Int -> Exp Int -> AccVector a
mergeCore' lt v len offset stride
  | len == 2
  = let x0 = getOffsetStride v offset stride 0
        x1 = getOffsetStride v offset stride 1
    in (x1 `lt` x0) ?| (genPair x1 x0, genPair x0 x1)

  | otherwise
  = let evens' = mergeCore' lt v (len `div` 2) offset            (stride * 2)
        odds'  = mergeCore' lt v (len `div` 2) (offset + stride) (stride * 2)
        v'	   = interleave evens' odds'
    in flipPairs lt v'

getOffsetStride :: (Elt a) => AccVector a -> Exp Int -> Exp Int -> Exp Int -> Exp a
getOffsetStride v offset stride ix = v ! ix'
  where
  ix' = index1 $ offset + (ix * stride)

compareAndSwap :: (Elt a) => (Exp a -> Exp a -> Exp Bool) -> Exp Int -> Exp Int -> AccVector a -> AccVector a
compareAndSwap cmp a b v = permute const v pF v
   where
   idxa = index1 a
   idxb = index1 b
   pF ix = let i = unindex1 ix in
           (i ==* a ||* i ==* b) ? (((v ! idxa) `cmp` (v !idxb)) ? ((i ==* a ? (idxb, idxa)) ,ignore) ,ignore)


-- | Interleaves two arrays. 
interleave :: (Elt a) => AccVector a -> AccVector a -> AccVector a
interleave xx yy = generate idx gF
  where
  idx = index1 $ size xx + size yy
  gF ix =
    let i  = unindex1 ix
        i' = index1 (i `div` 2) in
    (i `mod` 2 ==* 0) ? (xx ! i', yy ! i')

-- | Sorts adjacent pairs according to the comparison function.
--   lt a b has to return true if a < b.
--   Length has to be odd. The result for an even vector is undefined!
flipPairs :: (Elt a) => (Exp a -> Exp a -> Exp Bool) -> AccVector a -> AccVector a
flipPairs lt v = generate (shape v) gF
  where
  maxix = (size v) - 1
  --
  getval i =
    let bef = v ! index1 (i - 1) in
    let cur = v ! index1 i in
    let aft = v ! index1 (i + 1) in
    (i `mod` 2 ==* 0) ? (cur `lt` bef ? (bef, cur), (aft `lt` cur ? (aft, cur)))
  --
  gF ix =
     let i = unindex1 ix in
     (i ==* 0) ? (v ! ix, (i ==* maxix) ? (v ! ix, getval i))

{-



-- sorting algorithm
-- Stable?!
-- Is it possible to do this inplace?!
sortCore :: (Elt a) => (a -> a -> a) -> Exp Int -> Exp Int -> AccVector a -> AccVector a
sortCore cmp idxl idxr v =  (len ==* 0 ||* len ==* 1) ?| (v, v')
    where
    len = idxr - idxl
    half = len `div` 2
    v' = mergeCore idxl idxr (sortCore cmp 0 half (sortCore cmp half len v))

mergeCore :: (Elt a) => Exp Int -> Exp Int -> AccVector a -> AccVector a
mergeCore v = mergeCore' v 0 1



-- | Batcher odd/even merge.
--   The two lists to merge are appended on the input.
--   The length of the lists must be a power of two, else loop.
mergeCore :: [:Int:] -> [:Int:]
mergeCore xx = mergeCore' xx (lengthP xx) 0 1

mergeCore' :: [:Int:] -> Int -> Int -> Int -> [:Int:]
mergeCore' xx len offset stride
	| len == 2
	= let 	x0	= get xx offset stride 0
		x1	= get xx offset stride 1
	  in if x1 < x0
		then [: x1, x0 :]
		else [: x0, x1 :]
	
	| otherwise
	= let	evens'	= mergeCore' xx (len `div` 2) offset            (stride * 2)
		odds'	= mergeCore' xx (len `div` 2) (offset + stride) (stride * 2)
		xx'	= interleave evens' odds'
		ixLast	= lengthP xx' - 1

	  in	[: xx' !: 0 :]  
	    +:+ (flipPairs (sliceP 1 ixLast xx'))
	    +:+ [: xx' !: ixLast :]


-- | Get an indexed value from an array using an offset and stride.
get :: [:Int:] -> Int -> Int -> Int -> Int
get xx offset stride ix
	= xx !: (offset + (ix * stride))


-- | For each consecutive pair of elements, 
--	if they are out of order then flip them so they are.
flipPairs  :: [:Int:] -> [:Int:]
flipPairs xx
 = concatP 
	[: if y < x then [: y, x :] else [: x, y :]
	|  (x, y) 	<- oddevens xx :]


-- | Interleave the elements of two arrays.
interleave :: [:Int:] -> [:Int:] -> [:Int:]
interleave xx yy
 = concatP [: [:x, y:] | (x, y) <- zipP xx yy :]


-- | Pair up the elements with odd an even indices
--   oddevens [: 1, 8, 3, 6, 2, 8 :] 
--	= [: (1, 8), (3, 6), (2, 8) :]
--
oddevens :: [:Int:] -> [:(Int, Int):]
oddevens xx
	= [: (x, xx !: (ix + 1) )	
			| (ix, x)	<- indexedP xx
			, mod ix 2 == 0 :]
-}
