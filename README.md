# Cloning

To clone the repository, simply run

    $ git clone git@bitbucket.org:rjakob/accelerate-cloth.git
 
This creates a directory named accelerate-cloth in your current directory.

    $ cd accelerate-cloth

After cloning the repository, you have to set up the submodules by

    $ git submodule init
    $ git submodule update


# Build & Install
You can either build the system manually or automatically.

Requirements are:
* Internet connection (Dependencies are fetched from hackage)
* cabal-dev

## Automatic build
An automatic build uses cabal-dev to run a local build. The process
itself may take a while, as all the dependencies need to be fetched.

    $ ./autobuild.sh

The autobuild process creates all the submodules without documentation.
If you need documentation, change the variable CABALDEVARGS in autobuild.sh.

## Manual build
To build this system, cabal-dev is recommended (although it should
work with cabal, too).

Create a directory cabal-dev in the root dir of the project

    $ cd accelerate && cabal-dev -s ../cabal-dev install && cd ../
    $ cd accelerate/accelerate-<submodule> && cabal-dev -s ../../cabal-dev install && cd ../..
    $ cd accelerate-matrices && cabal-dev -s ../cabal-dev install && cd ../
    $ cabal-dev install


# Misc

To create a triangulated rectangle surface on coordinates 0-100 in x and y coordinates:

                   -x  +x -y  +y 
  ./rectsurface.sh  0 100  0 100 | qhull d Qt Ft

(prerequisites: qhull)
