#!/bin/bash

CABALDEV=cabal-dev
CABALDEVARGS=--disable-documentation

mkdir $CABALDEV
(cd accelerate && cabal-dev -s ../$CABALDEV install $CABALDEVARGS)
(cd accelerate-matrices && cabal-dev -s ../$CABALDEV install $CABALDEVARGS)
cabal-dev -s $CABALDEV install $CABALDEVARGS

echo "Now you can run the simulation by executing 'cabal-dev/bin/accelerate-cloth'"
